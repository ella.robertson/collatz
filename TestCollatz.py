#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    collatz_single_num_eval,
    compare_cached_cycles,
    collatz_max_cycle,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1000000)
        self.assertEqual(v, 525)

    def test_eval_6(self):
        v = collatz_eval(768, 900)
        self.assertEqual(v, 179)

    def test_eval_7(self):
        v = collatz_eval(89000, 17000)
        self.assertEqual(v, 351)

    # -----
    # compare cached cycles
    # -----

    def test_compare_cached_1(self):
        v = compare_cached_cycles(5, 10, -1)
        self.assertEqual(v, 268)

    def test_compare_cached_2(self):
        v = compare_cached_cycles(25, 40, 300)
        self.assertEqual(v, 324)

    # -----
    # max cycle
    # -----

    def test_max_cycle_1(self):
        v = collatz_max_cycle(2, 70002)
        self.assertEqual(v, 340)

    def test_max_cycle_2(self):
        v = collatz_max_cycle(123423, 900000)
        self.assertEqual(v, 525)

    # -----
    # single num
    # -----

    def test_single_num_1(self):
        v = collatz_single_num_eval(500)
        self.assertEqual(v, 111)

    def test_single_num_2(self):
        v = collatz_single_num_eval(51123)
        self.assertEqual(v, 128)

    def test_single_num_3(self):
        v = collatz_single_num_eval(990182)
        self.assertEqual(v, 91)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
